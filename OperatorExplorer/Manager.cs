﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorExplorer
{
    public class Manager : INotifyPropertyChanged
    {
        string item1Name;
        public string Item1Name
        {
            get { return item1Name; }
            set
            {
                item1Name = value;
                OnPropertyChanged(nameof(Item1Name));
            }
        }

        double item1Cost;
        public double Item1Cost
        {
            get { return item1Cost; }
            set
            {
                item1Cost = value;
                OnPropertyChanged(nameof(Item1Cost));
            }
        }

        double item1Weight;
        public double Item1Weight
        {
            get { return item1Weight; }
            set
            {
                item1Weight = value;
                OnPropertyChanged(nameof(Item1Weight));
            }
        }

        string item2Name;
        public string Item2Name
        {
            get { return item2Name; }
            set
            {
                item2Name = value;
                OnPropertyChanged(nameof(Item2Name));
            }
        }

        double item2Cost;
        public double Item2Cost
        {
            get { return item2Cost; }
            set
            {
                item2Cost = value;
                OnPropertyChanged(nameof(Item2Cost));
            }
        }

        double item2Weight;
        public double Item2Weight
        {
            get { return item2Weight; }
            set
            {
                item2Weight = value;
                OnPropertyChanged(nameof(Item2Weight));
            }
        }

        string itemOut;
        public string ItemOut
        {
            get { return itemOut; }
            set
            {
                itemOut = value;
                OnPropertyChanged(nameof(ItemOut));
            }
        }

        int itemMultiplication;
        public int ItemMultiplication
        {
            get { return itemMultiplication; }
            set
            {
                itemMultiplication = value;
                OnPropertyChanged(nameof(ItemMultiplication));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Manager()
        {
            Item1Name = "Example1";
            Item1Cost = 10.00;
            Item1Weight = 100.0;

            Item2Name = "Example2";
            Item2Cost = 20.00;
            Item2Weight = 50.0;

            ItemMultiplication = 0;

            ItemOut = "";
        }

public void Add()
{
    AddByBoxing(Item1Name, Item1Cost, Item1Weight, item2Name, Item2Cost, Item2Weight);
}

private void AddByBoxing(object a, object b, object c, object d, object e, object f)
{
    string name1 = (string)a;
    double cost1 = (double)b;
    double weight1 = (double)c;

    string name2 = (string)d;
    double cost2 = (double)e;
    double weight2 = (double)f;

    var item1 = new Item(name1, cost1, weight1);
    var item2 = new Item(name2, cost2, weight2);
    var item3 = item1 + item2;

    ItemOut = item3.ToString();
}

        public void Multiply()
        {
            var item = ItemMultiplication * new Item(Item1Name, Item1Cost, Item1Weight);

            ItemOut = item.ToString();
        }

        public void Equal()
        {
            bool comparison = new Item(Item1Name, Item1Cost, Item1Weight) == new Item(Item2Name, Item2Cost, Item2Weight);
            ItemOut = comparison.ToString();
        }

        public void NotEqual()
        {
            bool comparison = new Item(Item1Name, Item1Cost, Item1Weight) != new Item(Item2Name, Item2Cost, Item2Weight);
            ItemOut = comparison.ToString();
        }

        public void GreaterThan()
        {
            bool comparison = new Item(Item1Name, Item1Cost, Item1Weight)  > new Item(Item2Name, Item2Cost, Item2Weight);
            ItemOut = comparison.ToString();
        }

        public void LessThan()
        {
            bool comparison = new Item(Item1Name, Item1Cost, Item1Weight) < new Item(Item2Name, Item2Cost, Item2Weight);
            ItemOut = comparison.ToString();
        }

        public void GreaterThanEqual()
        {
            bool comparison = new Item(Item1Name, Item1Cost, Item1Weight) >= new Item(Item2Name, Item2Cost, Item2Weight);
            ItemOut = comparison.ToString();
        }

        public void LessThanEqual()
        {
            bool comparison = new Item(Item1Name, Item1Cost, Item1Weight) <= new Item(Item2Name, Item2Cost, Item2Weight);
            ItemOut = comparison.ToString();
        }

        public void CastToUint()
        {
            Item item = new Item(Item1Name, Item1Cost, Item1Weight);
            uint cost = (uint)item;
            ItemOut = cost.ToString();
        }

        public void CastToItem()
        {
            uint cost = (uint)ItemMultiplication;
            Item item = (Item)cost;
            ItemOut = item.ToString();
        }

        private void OnPropertyChanged(string property)
        {
            var propertyChanged = PropertyChanged;
            if(propertyChanged != null)
            {
                propertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
