﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OperatorExplorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.Add();
        }

        private void multiButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.Multiply();
        }

        private void eqButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.Equal();
        }

        private void neqButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.NotEqual();
        }

        private void gteButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.GreaterThanEqual();
        }

        private void gtButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.GreaterThan();
        }

        private void lteButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.LessThanEqual();
        }

        private void ltButton_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.LessThan();
        }

        private void castItem_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.CastToUint();
        }

        private void castUint_Click(object sender, RoutedEventArgs e)
        {
            var manager = FindResource("manager") as Manager;
            manager.CastToItem();
        }
    }
}
