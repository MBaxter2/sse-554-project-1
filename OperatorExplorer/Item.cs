﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OperatorExplorer
{
    public class Item
    {
        private string name;
        public string Name { get { return name; } }

        private double cost;
        public double Cost { get { return cost; } }

        private double weight;
        public double Weight { get { return weight; } }
    
        public Item(string name, double cost, double weight)
        {
            this.name = name;
            this.cost = cost;
            this.weight = weight;
        }

        public Item(Item i)
        {
            this.name = i.name;
            this.cost = i.cost;
            this.weight = i.weight;
        }

        public static Item operator +(Item left, Item right) => new Item(left.Name + " and " + right.Name, left.Cost + right.Cost, left.Weight + right.Weight);

        public static Item operator *(int left, Item right) => new Item(left + " " + right.Name, left * right.Cost, left * right.Weight);

        public static bool operator ==(Item left, Item right)
        {
            if (object.ReferenceEquals(left, right)) return true;

            return left.Name == right.Name && left.cost == right.cost && left.weight == right.weight; 
        }

        public static bool operator !=(Item left, Item right) => !(left == right);

        public static bool operator >=(Item left, Item right)
        {
            return left.Cost >= right.Cost;
        }

        public static bool operator <=(Item left, Item right)
        {
            return left.Cost <= right.Cost;
        }

        public static bool operator >(Item left, Item right)
        {
            return left.Cost > right.Cost;
        }

        public static bool operator <(Item left, Item right)
        {
            return left.Cost < right.Cost;
        }

        public static explicit operator uint(Item item)
        {
            checked
            {
                uint cost = Convert.ToUInt32(item.cost);
                return cost;
            }
        }

        public static explicit operator Item(uint cost)
        {       
            return new OperatorExplorer.Item("Unknown item", (double)cost, 0);          
        }

        public override string ToString()
        {
            return name + ", cost " + cost + ", and weighs " + weight;
        }
    }
}
