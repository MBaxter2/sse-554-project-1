﻿using System;
using Xunit;
using MemoryManager;

namespace Tests
{
    public class BlockTests
    {
        [Fact]
        public void Test1() 
        {
            Block testBlock = new Block() { };

            Assert.Equal(0, testBlock.A);
            Assert.Equal(0, testBlock.B);
            Assert.Equal(0, testBlock.B);
        }

        [Fact]
        public void Test2()
        {
            Block testBlock = new Block() { A = 10, B = 11, C = 12 };

            Assert.Equal(10, testBlock.A);
            Assert.Equal(11, testBlock.B);
            Assert.Equal(12, testBlock.C);
        }

        [Fact]
        public void Test3()
        {
            Block testBlock = new Block() { A = 1, B = 1, C = 11 };
            testBlock.CalcD();

            Assert.Equal(-9, testBlock.D);
        }

        [Fact]
        public void Test4()
        {
            Block testBlock = new Block() { A = 6, B = 5, C = 4 };       

            Assert.Equal(testBlock.ToString(), "[ A = 6, B = 5, C = 4 ]");
        }

        [Fact]
        public void Test5()
        {
            Block testBlock1 = new Block() { A = 6, B = 5, C = 4 };
            Block testBlock2 = new Block() { A = 6, B = 5, C = 4 };

            testBlock1.CalcD();
            testBlock2.CalcD();

            Assert.Equal(true, testBlock1.Equals(testBlock2));
        }
    }

    public class SafeClassTests
    {
        [Fact]
        public void Test1()
        {
            SafeClass testSafeClass = new SafeClass();

            double value = testSafeClass.RunCalculation();

            Assert.Equal(-45, value);            
        }

        [Fact]
        public void Test2()
        {
            SafeClass testSafeClass = new SafeClass();

            double value = testSafeClass.CalculateBlocks();

            Assert.Equal(0, value);
        }

        [Fact]
        public void Test3()
        {
            SafeClass testSafeClass = new SafeClass();

            var block = testSafeClass.ReferenceMaker();

            if(block.IsAlive)
            {
                var strongBlockRef = block.Target as Block;

                Assert.Equal(1, strongBlockRef.A);
                Assert.Equal(10, strongBlockRef.B);
                Assert.Equal(100, strongBlockRef.C);
            } 
        }
    }

    public class DanagerClassTests
    {
        [Fact]
        public unsafe void Test1()
        {
            DangerClass testDanagerClass = new DangerClass();

            int result = 0;
            int error = 0;

            error = testDanagerClass.CStyleAddFunction(10, 90, &result);

            Assert.Equal(100, result);
            Assert.Equal(error, 0);
        }

        [Fact]
        public unsafe void Test2()
        {
            DangerClass testDanagerClass = new DangerClass();

            int *result = null;
            int error = 0;

            error = testDanagerClass.CStyleAddFunction(10, 90, result);

            Assert.Equal(error, 1);
        }

        [Fact]
        public void Test3()
        {
            DangerClass testDanagerClass = new DangerClass();

            double result;

            result = testDanagerClass.StackArrayAverager(10);

            Assert.InRange(result, 0, 100);
        }

        [Fact]
        public void Test4()
        {
            DangerClass testDanagerClass = new DangerClass();

            double result;

            result = testDanagerClass.HeapArrayAverager(10);

            Assert.InRange(result, 0, 100);
        }
    }
}
