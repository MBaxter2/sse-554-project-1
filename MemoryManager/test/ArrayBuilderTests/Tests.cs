﻿using System;
using Xunit;
using ArrayBuilder;
using MemoryManager;

namespace Tests
{
    public class ArrayBuilder2DTests
    {
        [Fact]
        public void Test1() 
        {
            ArrayBuilder2D underTest = new ArrayBuilder2D(5);

            int[] newRow = new int[] { 1, 2, 3, 4 };
            underTest.AppendRow(newRow);

            Assert.Equal(1, underTest.Array.GetLength(0));
            Assert.Equal(5, underTest.Array.GetLength(1));
        }

        [Fact]
        public void Test2()
        {
            ArrayBuilder2D underTest = new ArrayBuilder2D(2);

            int[] newRow1 = { 1, 2 };
            int[] newRow2 = { 3, 4 };
            int[] newRow3 = { 5, 6 };

            int[,] expected = { { 1, 2}, { 3, 4}, { 5, 6} };

            underTest.AppendRow(newRow1);
            underTest.AppendRow(newRow2);
            underTest.AppendRow(newRow3);

            for(int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 2; j++)
                {
                    Assert.Equal(expected[i, j] , underTest.Array[i, j]);
                }
            }
        }

        [Fact]
        public void Test3()
        {
            ArrayBuilder2D underTest = new ArrayBuilder2D(5);

            int[] newRow = new int[] { 1, 2, 3, 4 };
            underTest.AppendRow(newRow);
            underTest.AppendRow(newRow);
            underTest.AppendRow(newRow);

            Tuple<int, int> dimensions = underTest.GetDimensions();

            Assert.Equal(dimensions.Item1, 5);
            Assert.Equal(dimensions.Item2, 3);
        }
    }

    public class ArrayBuilder3DTests
    {
        [Fact]
        public void Test1()
        {
            ArrayBuilder3D underTest = new ArrayBuilder3D(1, 1);

            int[,] array = { { 1, 2 } };
            underTest.Append2DArray(array);

            Assert.Equal(1, underTest.Array.GetLength(0));
            Assert.Equal(1, underTest.Array.GetLength(1));
            Assert.Equal(1, underTest.Array.GetLength(2));
        }

        [Fact]
        public void Test2()
        {
            ArrayBuilder3D underTest = new ArrayBuilder3D(2, 2);

            int[,] newArray1 = { { 1, 2 }, { 3, 4 } };
            int[,] newArray2 = { { 5, 6 }, { 7, 8 } };

            int[,,] expected = { { { 1, 2 }, { 3, 4 }, }, { { 5, 6 }, { 7, 8 } } };

            underTest.Append2DArray(newArray1);
            underTest.Append2DArray(newArray2);

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for(int k = 0; k < 2; k++)
                    {
                        Assert.Equal(expected[i, j, k], underTest.Array[i, j, k]);
                    }
                }
            }
        }

        [Fact]
        public void Test3()
        {
            ArrayBuilder3D underTest = new ArrayBuilder3D(2, 2);

            int[,] newArray = { { 1, 2 }, { 3, 4 } };

            underTest.Append2DArray(newArray);
            underTest.Append2DArray(newArray);
            underTest.Append2DArray(newArray);
            underTest.Append2DArray(newArray);

            Tuple<int, int, int> dimensions = underTest.GetDimensions();

            Assert.Equal(dimensions.Item1, 2);
            Assert.Equal(dimensions.Item2, 2);
            Assert.Equal(dimensions.Item3, 4);
        }
    }

    public class JaggedArrayBuilderTests
    {
        [Fact]
        public void Test1()
        {
            JaggedArrayBuilder underTest = new JaggedArrayBuilder();

            int[] row1 = { 1, 2, 3, 4 };
            int[] row2 = { 5, 6, 7 };
            underTest.AppendRow(row1);
            underTest.AppendRow(row2);

            Assert.Equal(4, underTest.Array[0].Length);
            Assert.Equal(3, underTest.Array[1].Length);
        }

        [Fact]
        public void Test2()
        {
            JaggedArrayBuilder underTest = new JaggedArrayBuilder();

            int[] row1 = { 1, 2, 3, 4 };
            int[] row2 = { 5, 6, 7 };
            int[] row3 = { 8, 9, 10, 11, 12, 13 };

            int[][] expected = new int[3][];
            expected[0] = row1;
            expected[1] = row2;
            expected[2] = row3;

            underTest.AppendRow(row1);
            underTest.AppendRow(row2);
            underTest.AppendRow(row3);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < expected[i].Length; j++)
                {
                    Assert.Equal(expected[i][j], underTest.Array[i][j]);              
                }
            }
        }
    }

    public class BlockCollectionTests
    {
        [Fact]
        public void Test1()
        {
            BlockCollection underTest = new BlockCollection();

            int count = 0;

            foreach(Block b in underTest)
            {
                count++;
                Assert.InRange(b.A, 0, 10);
                Assert.InRange(b.B, 0, 10);
                Assert.InRange(b.C, 0, 10);
            }

            Assert.InRange(count, 0, 6);
        }

        [Fact]
        public void Test2()
        {
            BlockCollection underTest = new BlockCollection();

            int count = 0;

            foreach (double d in underTest.EnumerateD())
            {
                count++;
                Assert.InRange(d, -10, 20);
            }

            Assert.InRange(count, 1, 12);
        }

        [Fact]
        public void Test3()
        {
            Block[] blocks = new Block[]
            {
                new Block { A = 1, B = 1, C = 1},
                new Block { A = 2, B = 2, C = 2},
                new Block { A = 3, B = 3, C = 3}
            };
            BlockCollection underTest = new BlockCollection(blocks);

            for(int i = 0; i < 3; i++)
            {
                Assert.Equal((double)(i+1), underTest[i].A);
            }
        }

        [Fact]
        public void Test4()
        {
            Block[] blocks = new Block[]
            {
                new Block { A = 1, B = 1, C = 1},
                new Block { A = 2, B = 2, C = 2},
                new Block { A = 3, B = 3, C = 3}
            };
            BlockCollection underTest = new BlockCollection(blocks);

            int count = 0;
            foreach(Block b in underTest[(double)2])
            {
                Assert.Equal((double)2, b.A);
                count++;
            }
            Assert.Equal(1, count);
        }
    }
}
