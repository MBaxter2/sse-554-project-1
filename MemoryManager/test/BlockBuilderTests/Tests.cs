﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Xunit;
using BlockBuilder;
using MemoryManager;

namespace Tests
{
    public class BlockBuilder
    {
        [Fact]
        public void Test1() 
        {
            int count = 2;
            var queueA = new BlockingCollection<int>();
            var queueB = new BlockingCollection<int>();
            var queueC = new BlockingCollection<int>();

            BlockBuilderPipline bbp = new BlockBuilderPipline();

            Task.WaitAll(bbp.DataGenerator(count, queueA, queueB, queueC));
            
            Assert.Equal(2, queueA.Count);
            Assert.Equal(2, queueB.Count);
            Assert.Equal(2, queueC.Count);

            foreach(int i in queueA)
            {
                Assert.InRange(i, 0, 10);
            }
            foreach (int i in queueB)
            {
                Assert.InRange(i, 0, 10);
            }
            foreach (int i in queueB)
            {
                Assert.InRange(i, 0, 10);
            }
        }

        [Fact]
        public void Test2()
        {
            var queueA = new BlockingCollection<int>();
            var stageA = new BlockingCollection<Block>();

            queueA.Add(1);
            queueA.Add(1);
            queueA.CompleteAdding();

            BlockBuilderPipline bbp = new BlockBuilderPipline();

            Task.WaitAll(bbp.BlockBuilderStageA(queueA, stageA));

            Assert.Equal(2, stageA.Count);
            
            foreach (Block b in stageA)
            {
                Assert.Equal(b.A, 1);
            }
        }

        [Fact]
        public void Test3()
        {
            var queueB = new BlockingCollection<int>();
            var stageA = new BlockingCollection<Block>();
            var stageB = new BlockingCollection<Block>();

            queueB.Add(2);
            queueB.Add(2);
            queueB.CompleteAdding();
            stageA.Add(new Block());
            stageA.Add(new Block());
            stageA.CompleteAdding();

            BlockBuilderPipline bbp = new BlockBuilderPipline();

            Task.WaitAll(bbp.BlockBuilderStageB(queueB, stageA, stageB));

            Assert.Equal(2, stageB.Count);

            foreach (Block b in stageB)
            {
                Assert.Equal(b.B, 2);
            }
        }

        [Fact]
        public void Test4()
        {
            var queueC = new BlockingCollection<int>();
            var stageB = new BlockingCollection<Block>();
            var stageC = new BlockingCollection<Block>();

            queueC.Add(3);
            queueC.Add(3);
            queueC.CompleteAdding();
            stageB.Add(new Block());
            stageB.Add(new Block());
            stageB.CompleteAdding();

            BlockBuilderPipline bbp = new BlockBuilderPipline();

            Task.WaitAll(bbp.BlockBuilderStageC(queueC, stageB, stageC));

            Assert.Equal(2, stageC.Count);

            foreach (Block c in stageC)
            {
                Assert.Equal(c.C, 3);
            }
        }
    }
}
