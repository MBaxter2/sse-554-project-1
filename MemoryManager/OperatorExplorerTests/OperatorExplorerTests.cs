﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OperatorExplorer;

namespace OperatorExplorerTests
{
    [TestClass]
    public class ItemUnitTests
    {
        [TestMethod]
        public void ItemUnitTest1()
        {
            Item item = new Item("Test", 10, 100);

            Assert.AreEqual("Test", item.Name);
            Assert.AreEqual(10, item.Cost);
            Assert.AreEqual(100, item.Weight);   
        }

        [TestMethod]
        public void ItemUnitTest2()
        {
            Item item = new Item("Test", 10, 100);

            Assert.AreEqual("Test, cost 10, and weighs 100", item.ToString());
        }

        [TestMethod]
        public void ItemUnitTest3()
        {
            Item item1 = new Item("Test1", 10, 100);
            Item item2 = new Item("Test2", 5, 10);
            Item item3 = item1 + item2;

            Assert.AreEqual("Test1 and Test2", item3.Name);
            Assert.AreEqual(15, item3.Cost);
            Assert.AreEqual(110, item3.Weight);
        }

        [TestMethod]
        public void ItemUnitTest4()
        {
            Item item1 = new Item("Test1", 4, 8);
            Item item2 = 2 * item1;

            Assert.AreEqual("2 Test1", item2.Name);
            Assert.AreEqual(8, item2.Cost);
            Assert.AreEqual(16, item2.Weight);
        }

        [TestMethod]
        public void ItemUnitTest5()
        {
            Item item1 = new Item("Test1", 10, 100);
            Item item2 = new Item("Test2", 5, 10);
            Item item3 = new Item("Test3", 10, 100);
            Item item4 = new Item("Test4", 10, 20);
            Item item5 = new Item("Test1", 10, 100);
            Item item6 = new Item("Test5", 5, 100);

            bool result1 = item1 == item2;
            bool result2 = item1 == item3;
            bool result3 = item1 == item4;
            bool result4 = item1 == item5;
            bool result5 = item1 == item6;

            Assert.AreEqual(false, result1);
            Assert.AreEqual(false, result2);
            Assert.AreEqual(false, result3);
            Assert.AreEqual(true, result4);
            Assert.AreEqual(false, result5);
        }

        [TestMethod]
        public void ItemUnitTest6()
        {
            Item item1 = new Item("Test1", 10, 100);
            Item item2 = new Item("Test2", 5, 10);
            Item item3 = new Item("Test3", 10, 100);
            Item item4 = new Item("Test4", 10, 20);
            Item item5 = new Item("Test1", 10, 100);
            Item item6 = new Item("Test5", 5, 100);

            bool result1 = item1 != item2;
            bool result2 = item1 != item3;
            bool result3 = item1 != item4;
            bool result4 = item1 != item5;
            bool result5 = item1 != item6;

            Assert.AreEqual(true, result1);
            Assert.AreEqual(true, result2);
            Assert.AreEqual(true, result3);
            Assert.AreEqual(false, result4);
            Assert.AreEqual(true, result5);
        }

        [TestMethod]
        public void ItemUnitTest7()
        {
            Item item1 = new Item("Test1", 10, 100);
            Item item2 = new Item("Test2", 10, 99);
            Item item3 = new Item("Test3", 11, 10);
            Item item4 = new Item("Test4", 9, 100);

            bool result1 = item1 <= item2;
            bool result2 = item1 <= item3;
            bool result3 = item1 <= item4;

            Assert.AreEqual(true, result1);
            Assert.AreEqual(true, result2);
            Assert.AreEqual(false, result3);
        }

        [TestMethod]
        public void ItemUnitTest8()
        {
            Item item1 = new Item("Test1", 10, 100);
            Item item2 = new Item("Test2", 10, 99);
            Item item3 = new Item("Test3", 11, 10);
            Item item4 = new Item("Test4", 9, 100);

            bool result1 = item1 >= item2;
            bool result2 = item1 >= item3;
            bool result3 = item1 >= item4;

            Assert.AreEqual(true, result1);
            Assert.AreEqual(false, result2);
            Assert.AreEqual(true, result3);
        }

        [TestMethod]
        public void ItemUnitTest9()
        {
            Item item1 = new Item("Test1", 10, 100);
            Item item2 = new Item("Test2", 10, 99);
            Item item3 = new Item("Test3", 11, 10);
            Item item4 = new Item("Test4", 9, 100);

            bool result1 = item1 > item2;
            bool result2 = item1 > item3;
            bool result3 = item1 > item4;

            Assert.AreEqual(false, result1);
            Assert.AreEqual(false, result2);
            Assert.AreEqual(true, result3);
        }

        [TestMethod]
        public void ItemUnitTest10()
        {
            Item item1 = new Item("Test1", 10, 100);
            Item item2 = new Item("Test2", 10, 99);
            Item item3 = new Item("Test3", 11, 10);
            Item item4 = new Item("Test4", 9, 100);

            bool result1 = item1 < item2;
            bool result2 = item1 < item3;
            bool result3 = item1 < item4;

            Assert.AreEqual(false, result1);
            Assert.AreEqual(true, result2);
            Assert.AreEqual(false, result3);
        }

        [TestMethod]
        public void ItemUnitTest11()
        {
            Item item1 = new Item("Test1", 10.1, 100);
            uint cost = (uint)item1;
            
            Assert.AreEqual(cost, (uint)10);
        }

        [TestMethod]
        public void ItemUnitTest12()
        {
            uint cost = 100;
            Item item1 = (Item)cost;

            Assert.AreEqual("Unknown item", item1.Name);
            Assert.AreEqual(100, item1.Cost);
            Assert.AreEqual(0, item1.Weight);
        }
    }

    [TestClass]
    public class ManagerTests
    {
        [TestMethod]
        public void ManagerTest1()
        {
            Manager manager = new Manager();

            Assert.AreEqual("Example1", manager.Item1Name);
            Assert.AreEqual(10, manager.Item1Cost);
            Assert.AreEqual(100, manager.Item1Weight);
            Assert.AreEqual("Example2", manager.Item2Name);
            Assert.AreEqual(20, manager.Item2Cost);
            Assert.AreEqual(50, manager.Item2Weight);
            Assert.AreEqual(0, manager.ItemMultiplication);
            Assert.AreEqual("", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest2()
        {
            Manager manager = new Manager();
            manager.Add();

            Assert.AreEqual("Example1 and Example2, cost 30, and weighs 150", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest3()
        {
            Manager manager = new Manager();
            manager.Multiply();

            Assert.AreEqual("0 Example1, cost 0, and weighs 0", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest4()
        {
            Manager manager = new Manager();
            manager.Equal();

            Assert.AreEqual("False", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest5()
        {
            Manager manager = new Manager();
            manager.NotEqual();

            Assert.AreEqual("True", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest6()
        {
            Manager manager = new Manager();
            manager.GreaterThan();

            Assert.AreEqual("False", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest7()
        {
            Manager manager = new Manager();
            manager.LessThan();

            Assert.AreEqual("True", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest8()
        {
            Manager manager = new Manager();
            manager.GreaterThanEqual();

            Assert.AreEqual("False", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest9()
        {
            Manager manager = new Manager();
            manager.LessThanEqual();

            Assert.AreEqual("True", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest10()
        {
            Manager manager = new Manager();
            manager.CastToUint();

            Assert.AreEqual("10", manager.ItemOut);
        }

        [TestMethod]
        public void ManagerTest11()
        {
            Manager manager = new Manager();
            manager.CastToItem();

            Assert.AreEqual("Unknown item, cost 0, and weighs 0", manager.ItemOut);
        }
    }
}
