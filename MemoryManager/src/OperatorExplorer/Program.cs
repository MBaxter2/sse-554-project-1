﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Immutable;

namespace Counter
{
    public class Program
    {
        static ImmutableArray<int> hugeImmutableArray;
        static int counter1 = 0;
        static int counter2 = 0;
        static int counter3 = 0;

        public static void Main(string[] args)
        {
            try
            {
                checkedCounter(2, 500);
            }
            catch(OverflowException e)
            {
                System.Console.WriteLine(Environment.NewLine + e.Message);
            }

            System.Console.WriteLine(Environment.NewLine);
            try
            {
                uncheckedCounter(2, 500);
            }
            catch (OverflowException e)
            {
                System.Console.WriteLine(Environment.NewLine + e.Message);
            }

            Random r = new Random();
            int[] hugeArray = new int[10000000];
            for(int i = 0; i < 10000000; i++)
            {
                hugeArray[i] = r.Next(0, 1000);
            }
            hugeImmutableArray = hugeArray.ToImmutableArray();
            
            Thread t1 = new Thread(threadedCounter1);
            Thread t2 = new Thread(threadedCounter2);
            Thread t3 = new Thread(threadedCounter3);

            System.Console.WriteLine(Environment.NewLine + "Starting search of immutable array...");
            t1.Start();
            t2.Start();
            t3.Start();

            t1.Join();
            t2.Join();
            t3.Join();

            System.Console.WriteLine("The value 10 was found {0} times.", counter1);
            System.Console.WriteLine("The value 20 was found {0} times.", counter2);
            System.Console.WriteLine("The value 30 was found {0} times.", counter3);

            System.Console.ReadKey();
        }

        public static void checkedCounter(int step, int length)
        {
            byte counter = 0;
            int lineCount = 20;

            System.Console.WriteLine("Begin checked counting...");
            for (int i = 0; i < length; i++)
            {
                if (i % lineCount == lineCount - 1)
                    System.Console.WriteLine("");
                else if (i % lineCount != 0)
                    System.Console.Write(", {0,3}", counter);
                else
                    System.Console.Write("{0,3}", counter);

                checked
                {
                    counter += (byte)step;
                }
            }
        }

        public static void uncheckedCounter(int step, int length)
        {
            byte counter = 0;
            int lineCount = 20;

            System.Console.WriteLine("Begin unchecked counting...");
            for (int i = 0; i < length; i++)
            {
                if (i % lineCount == lineCount - 1)
                    System.Console.WriteLine("");
                else if (i % lineCount != 0)
                    System.Console.Write(", {0,3}", counter);
                else
                    System.Console.Write("{0,3}", counter);

                unchecked
                {
                    counter += (byte)step;
                }
            }
        }

        public static void threadedCounter1()
        {
            foreach(int a in hugeImmutableArray)
            {
                if (a == 10)
                    counter1++;
            }
        }

        public static void threadedCounter2()
        {
            foreach (int a in hugeImmutableArray)
            {
                if (a == 20)
                    counter2++;
            }
        }

        public static void threadedCounter3()
        {
            foreach (int a in hugeImmutableArray)
            {
                if (a == 30)
                    counter3++;
            }
        }
    }
}
