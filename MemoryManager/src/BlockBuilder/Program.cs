﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using MemoryManager;

namespace BlockBuilder
{
    public class Program
    {
        public static void Main(string[] args)
        {
            int count = 4;
            BlockBuilderPipline bbp = new BlockBuilderPipline();
            var queueA = new BlockingCollection<int>();
            var queueB = new BlockingCollection<int>();
            var queueC = new BlockingCollection<int>();
            var stageA = new BlockingCollection<Block>();
            var stageB = new BlockingCollection<Block>();
            var stageC = new BlockingCollection<Block>();

            Console.WriteLine($"Building {count} blocks...");
            Task t1 = bbp.DataGenerator(count, queueA, queueB, queueC);
            Task.WaitAll(t1);
            Task t2 = bbp.BlockBuilderStageA(queueA, stageA);
            Task t3 = bbp.BlockBuilderStageB(queueB, stageA, stageB);
            Task t4 = bbp.BlockBuilderStageC(queueC, stageB, stageC);
            Task t5 = bbp.PrintCompletedBlock(stageC);
            Task.WaitAll(t2, t3, t4, t5);
            Console.WriteLine($"Block building complete");
            Console.ReadKey();
        }
    }

    public class BlockBuilderPipline
    {         
        public BlockBuilderPipline()
        {
        }

        public Task DataGenerator(int count, BlockingCollection<int> queueA, BlockingCollection<int> queueB, BlockingCollection<int> queueC)
        {
            Random random = new Random();

            return Task.Factory.StartNew(() =>
            {
                for (int i = 0, j = 0, k = 0; i < count || j < count || k < count;)
                {
                    double r = random.NextDouble();

                    if(r < 0.4 && i < count)
                    {
                        int a = random.Next(0, 10);
                        Console.WriteLine($"Generator: Added {a} to Queue A");
                        queueA.Add(a);
                        i++;
                        if(i >= count)
                            queueA.CompleteAdding();
                    }
                    else if((r >= 0.4 && r < 0.8) && j < count)
                    {
                        int b = random.Next(0, 10);
                        Console.WriteLine($"Generator: Added {b} to Queue B");
                        queueB.Add(b);
                        j++;
                        if(j >= count)
                            queueB.CompleteAdding();
                    }
                    else if (k < count)
                    {
                        int c = random.Next(0, 10);
                        Console.WriteLine($"Generator: Added {c} to Queue C");
                        queueC.Add(c);
                        k++;
                        if(k >= count)
                            queueC.CompleteAdding();
                    }
                } 
            }, TaskCreationOptions.LongRunning);
        }

        public Task BlockBuilderStageA(BlockingCollection<int> queueA, BlockingCollection<Block> stageA)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (var a in queueA.GetConsumingEnumerable())
                {
                    Block newBlock = new Block();
                    newBlock.A = a;
                    stageA.Add(newBlock);
                    Console.WriteLine($"Stage A: Added {a} to {newBlock}");
                }

                stageA.CompleteAdding();
            }, TaskCreationOptions.LongRunning);
        }

        public Task BlockBuilderStageB(BlockingCollection<int> queueB, BlockingCollection<Block> stageA, BlockingCollection<Block> stageB)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (Block block in stageA.GetConsumingEnumerable())
                {
                    int b = 0;
                    while (queueB.TryTake(out b) == false) ;

                    block.B = b;
                    stageB.Add(block);
                    Console.WriteLine($"Stage B: Added {b} to {block}");
                }                

                stageB.CompleteAdding();
            }, TaskCreationOptions.LongRunning);
        }

        public Task BlockBuilderStageC(BlockingCollection<int> queueC, BlockingCollection<Block> stageB, BlockingCollection<Block> stageC)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (Block block in stageB.GetConsumingEnumerable())
                {
                    int c = 0;
                    while(queueC.TryTake(out c) == false);

                    block.C = c;
                    stageC.Add(block);
                    Console.WriteLine($"Stage C: Added {c} to {block}");
                }
              
                stageC.CompleteAdding();
            }, TaskCreationOptions.LongRunning);
        }

        public Task PrintCompletedBlock(BlockingCollection<Block> stageC)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (Block block in stageC.GetConsumingEnumerable())
                {
                    Console.WriteLine($"Finished Block: {block}");
                }
            }, TaskCreationOptions.LongRunning);
        }
    }
}
