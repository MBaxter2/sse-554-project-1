﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryManager
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SafeClass sc = new SafeClass();
            DangerClass dc = new DangerClass();

            sc.RunCalculation();
            System.Console.WriteLine("");

            sc.CalculateBlocks();
            System.Console.WriteLine("");

            sc.ReferenceChecker();
            System.Console.WriteLine("");

            unsafe
            {
                int result = 0;
                int error = 0;
                error = dc.CStyleAddFunction(100, 1000, &result);
                System.Console.WriteLine("100 + 1000 = {0}", result);
            }
            System.Console.WriteLine("");

            System.Console.WriteLine("Array averaging using stack:");
            var watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < 10000; i++)
            {
                dc.StackArrayAverager(100000);
            }
            var elapsedTime = watch.ElapsedMilliseconds;
            System.Console.WriteLine("Execution took {0}ms", elapsedTime);

            System.Console.WriteLine("Array averaging using heap:");
            watch = System.Diagnostics.Stopwatch.StartNew();
            for (int i = 0; i < 10000; i++)
            {
                dc.HeapArrayAverager(100000);
            }
            elapsedTime = watch.ElapsedMilliseconds;
            System.Console.WriteLine("Execution took {0}ms", elapsedTime);

            Block b1 = new Block() { A = 99, B = 88, C = 77 };
            Block b2 = new Block() { A = 99, B = 88, C = 77 };

            if(b1 == b2)
            {
                System.Console.WriteLine("Block {0} is the same reference as Block {1}", b1, b2);
            }
            else
            {
                System.Console.WriteLine("Block {0} is not the same reference as Block {1}", b1, b2);
            }

            if (b1.Equals(b2))
            {
                System.Console.WriteLine("Block {0} is equal to Block {1}", b1, b2);
            }
            else
            {
                System.Console.WriteLine("Block {0} is not equal to Block {1}", b1, b2);
            }

            System.Console.ReadKey();

        }
    }
}
