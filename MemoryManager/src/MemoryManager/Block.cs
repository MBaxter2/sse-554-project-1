﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;

namespace MemoryManager
{
public class Block : IEquatable<Block>
{
    public int A;
    public int B;
    public int C;

    private double d;
    public double D
    {
        get { return d; }
    }

    public void CalcD()
    {
        d = (double)A + (double)B - (double)C;
    }

    public bool Equals(Block other)
    {
        if (other == null)
            return base.Equals(other);

        return A == other.A && B == other.B && C == other.C;
    }

    public override int GetHashCode() => A.GetHashCode();

    public override string ToString()
    {
        return "[ A = " + A + ", B = " + B + ", C = " + C + " ]";
    }
}

    class BlockTupleComparer : IEqualityComparer
    {
        public new bool Equals(object x, object y) => x.Equals(y);

        public int GetHashCode(object obj) => obj.GetHashCode();
    }
}
