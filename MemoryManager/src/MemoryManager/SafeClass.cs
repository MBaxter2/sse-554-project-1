﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryManager
{
    public class SafeClass
    {
        public double RunCalculation()
        {
            int stackValue1 = 0;
            int stackValue2 = 100;
            int stackValue3;
            double stackValue4 = 5.0;
            double stackValue5 = 50.0;

            stackValue3 = stackValue1 + stackValue2;

            unsafe
            {
                System.Console.WriteLine("stackValue1 one is at: {0:X} ", (int)&stackValue1);
                System.Console.WriteLine("stackValue2 one is at: {0:X} ", (int)&stackValue2);
                System.Console.WriteLine("stackValue3 one is at: {0:X} ", (int)&stackValue3);
                System.Console.WriteLine("stackValue4 one is at: {0:X} ", (int)&stackValue4);
                System.Console.WriteLine("stackValue5 one is at: {0:X} ", (int)&stackValue5);
            }

            return stackValue4 + stackValue5 - (double)stackValue3;
        }

        public double CalculateBlocks()
        {
            Block firstBlock = new Block() { A = 1, B = 2, C = 3 };
            Block secondBlock = new Block() { A = 10, B = 20, C = 30 };
            Block thirdBlock;
            int newA;
            int newB;
            int newC;

            firstBlock.CalcD();
            secondBlock.CalcD();

            newA = (int)firstBlock.D;
            newB = (int)secondBlock.D;
            newC = (int)(firstBlock.D + secondBlock.D);

            thirdBlock = new Block() { A = newA, B = newB, C = newC };
            thirdBlock.CalcD();

            return thirdBlock.D;
        }

        public void ReferenceChecker()
        {
            var weakBlock = ReferenceMaker();

            System.Console.WriteLine("Reference to block is {0}", weakBlock.IsAlive ? "alive" : "not alive");
            System.GC.Collect();
            System.Console.WriteLine("Requesting garbage collection...");
            System.Console.WriteLine("Reference to block is {0}", weakBlock.IsAlive ? "alive" : "not alive");
        }

        public WeakReference ReferenceMaker()
        {
            Block block = new MemoryManager.Block() { A = 1, B = 10, C = 100 };

            return new WeakReference(block);
        }
    }
}
