﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MemoryManager
{
    public unsafe class DangerClass
    {
        Random random;

        public DangerClass()
        {
            random = new Random();
        }

        public int CStyleAddFunction(int a, int b, int *result)
        {
            int errorCode = 0;

            if(result != null)
            {
                *result = a + b;
            }
            else
            {
                errorCode = 1;
            }

            return errorCode;
        }

        public double StackArrayAverager(int size)
        {
            double* array = stackalloc double[size];
            double* ptr = array;
            double total = 0;

            for(int i = 0; i < size; i++)
            {
                array[i] = (double)random.Next(0, 101);
            }

            for(int i = 0; i < size; i++)
            {
                total += *ptr;
                ptr = array + i;
            }

            return total / (double)size;
        }

        public double HeapArrayAverager(int size)
        {
            double[] array = new double[size];
            double total = 0;

            for(int i = 0; i < size; i++)
            {
                array[i] = (double)random.Next(0, 101);
            }

            for (int i = 0; i < size; i++)
            {
                total += array[i];   
            }

            return total / (double)size;
        }
    }
}
