﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArrayBuilder
{
    public class ArrayBuilder2D
    {
        public int[,] Array;

        private int rowSize = 0;
        private int columnSize = 0;

        public ArrayBuilder2D(int rowSize)
        {
            Array = new int[0, 0];
            this.rowSize = rowSize;
            this.columnSize = 0;
        }

        public void AppendRow(int[] row)
        {
            columnSize += 1;
            int[,] newArray = new int[columnSize, rowSize];

            for(int i = 0; i < (columnSize - 1); i++)
            {
                for(int j = 0; j < rowSize; j++)
                {
                    newArray[i, j] = Array[i, j];
                }
            }

            for (int i = 0; i < rowSize; i++)
            {
                if (i < row.Count())
                {
                    newArray[columnSize - 1, i] = row[i];
                }
                else
                {
                    newArray[columnSize - 1, i] = 0;
                }
            }

            Array = newArray;
        }

        public Tuple<int, int> GetDimensions()
        {
            return Tuple.Create(rowSize, columnSize);
        }
    }
}
