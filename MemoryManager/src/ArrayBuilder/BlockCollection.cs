﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoryManager;

namespace ArrayBuilder
{
    public class BlockCollection
    {
        private Random random;
        private Block[] blocks;

        public IEnumerable<Block> this[double a]
        {
            get { return blocks.Where(b => b.A == a); }
        }

        public Block this[int index]
        {
            get { return blocks[index]; }
            set { blocks[index] = value; }
        }

        public BlockCollection()
        { 
            random = new Random();
        }

        public BlockCollection(params Block[] blocks)
        {
            this.blocks = blocks.ToArray();
        }

        public IEnumerator<Block> GetEnumerator()
        {
            int limit = random.Next(0, 6);
            for (int i = 0; i < limit; i++)
            {
                yield return new Block() { A = random.Next(0, 10), B = random.Next(0, 10), C = random.Next(0, 10) };
            }
        }

        public IEnumerable<double> EnumerateD()
        {
            int limit = random.Next(1, 12);
            for (int i = 0; i < limit; i++)
            {
                var Block = new Block() { A = random.Next(0, 10), B = random.Next(0, 10), C = random.Next(0, 10) };
                Block.CalcD();
                yield return Block.D;
            }
        }
    }
}
