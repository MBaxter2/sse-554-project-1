﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MemoryManager;


namespace ArrayBuilder
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ArrayBuilder2D builder1 = new ArrayBuilder2D(5);

            int[] row1 = new int[] { 0, 1, 2, 3, 4, 5 };
            int[] row2 = new int[] { 10, 11, 12, 13, 14, 15 };

            builder1.AppendRow(row1);
            builder1.AppendRow(row2);

            System.Console.WriteLine("2D Array Builder:");
            print2dArray(builder1.Array);
            System.Console.WriteLine(Environment.NewLine);

            ArrayBuilder3D builder2 = new ArrayBuilder3D(2, 2);

            int[,] array1 = new int[,] { { 0, 1 }, { 2, 3 } };
            int[,] array2 = new int[,] { { 4, 5 }, { 6, 7 } };

            builder2.Append2DArray(array1);
            builder2.Append2DArray(array2);

            System.Console.WriteLine("3D Array Builder:");
            print3dArray(builder2.Array);
            System.Console.WriteLine(Environment.NewLine);

            JaggedArrayBuilder builder3 = new JaggedArrayBuilder();

            int[] jaggedRow1 = new int[] { 0, 1, 2, 3 };
            int[] jaggedRow2 = new int[] { 0, 1, 2, 3, 4, 5, 6 };
            int[] jaggedRow3 = new int[] { 0, 1 };

            builder3.AppendRow(jaggedRow1);
            builder3.AppendRow(jaggedRow2);
            builder3.AppendRow(jaggedRow3);

            System.Console.WriteLine("Jagged Array Builder:");
            printJaggedArray(builder3.Array);
            System.Console.WriteLine(Environment.NewLine);

            BlockCollection blocks = new BlockCollection();

            System.Console.WriteLine("Random Blocks:");
            foreach (Block b in blocks)
            {
                System.Console.WriteLine(b);
            }
            System.Console.WriteLine(Environment.NewLine);

            System.Console.WriteLine("Random Blocks D Value:");
            foreach (double d in blocks.EnumerateD())
            {
                System.Console.WriteLine(d);
            }
            System.Console.WriteLine(Environment.NewLine);

            Block[] blockArray = new Block[] { new Block() { A = 1, B = 2, C = 3},
                new Block() { A = 6, B = 7, C = 8},
                new Block() { A = 1, B = 1, C = 1},
                new Block() { A = 9, B = 10, C = 11}};

            blocks = new BlockCollection(blockArray);

            System.Console.Write("The second element in block collection is: ");
            System.Console.WriteLine(blocks[1]);

            System.Console.WriteLine("The following blocks have an 'A' value of 1: ");
            foreach (Block b in blocks[(double)1])
            {
                System.Console.WriteLine(b);
            }

            System.Console.ReadKey();
        }

        private static void print2dArray(int[,] array)
        {          
            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (i == 0)
                {
                    System.Console.Write("[ ");
                }
                else
                {
                    System.Console.Write("  ");
                }
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        System.Console.Write("[ ");
                        System.Console.Write(array[i, j]);
                    }
                    else if (j == array.GetLength(1) - 1)
                    {
                        System.Console.Write(", ");
                        System.Console.Write(array[i, j]);
                        System.Console.Write(" ]");
                    }
                    else
                    {
                        System.Console.Write(", ");
                        System.Console.Write(array[i, j]);
                    }
                }
                if(i == array.GetLength(0) - 1)
                {
                    System.Console.Write(" ]");
                }
                else
                {
                    System.Console.WriteLine(", ");
                }
            }
        }

        private static void print3dArray(int[,,] array)
        {
            for (int i = 0; i < array.GetLength(0); i++)
            {
                if (i == 0)
                {
                    System.Console.Write("[ ");
                }
                else
                {
                    System.Console.Write("  ");
                }
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (j == 0)
                    {
                        System.Console.Write("[ ");
                    }
                    else
                    {
                        System.Console.Write("  ");
                    }
                    for (int k = 0; k < array.GetLength(2); k++)
                    {
                        if (k == 0)
                        {
                            System.Console.Write("[ ");
                            System.Console.Write(array[i, j, k]);
                        }
                        else if (k == array.GetLength(2) - 1)
                        {
                            System.Console.Write(", ");
                            System.Console.Write(array[i, j, k]);
                            System.Console.Write(" ]");
                        }
                        else
                        {
                            System.Console.Write(", ");
                            System.Console.Write(array[i, j, k]);
                        }
                    }
                    if (j == array.GetLength(1) - 1)
                    {
                        System.Console.Write(" ]");
                    }
                    else
                    {
                        System.Console.Write(", ");
                    }
                }
                if (i == array.GetLength(0) - 1)
                {
                    System.Console.Write(" ]");
                }
                else
                {
                    System.Console.WriteLine(", ");
                }
            }
        }

        private static void printJaggedArray(int[][] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0)
                {
                    System.Console.Write("[ ");
                }
                else
                {
                    System.Console.Write("  ");
                }                
                for (int j = 0; j < array[i].Length; j++)
                {
                    if (j == 0)
                    {
                        System.Console.Write("[ ");
                        System.Console.Write(array[i][j]);
                    }
                    else if (j == array[i].Length - 1)
                    {
                        System.Console.Write(", ");
                        System.Console.Write(array[i][j]);
                        System.Console.Write(" ]");
                    }
                    else
                    {
                        System.Console.Write(", ");
                        System.Console.Write(array[i][j]);
                    }
                }
                if (i == array.Length - 1)
                {
                    System.Console.Write(" ]");
                }
                else
                {
                    System.Console.WriteLine(", ");
                }
            }
        }


    }
}
