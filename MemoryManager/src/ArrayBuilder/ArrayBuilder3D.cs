﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArrayBuilder
{
    public class ArrayBuilder3D
    {
        public int[,,] Array;

        private int rowSize = 0;
        private int columnSize = 0;
        private int depthSize = 0;

        public ArrayBuilder3D(int rowSize, int columnSize)
        {
            Array = new int[0, 0, 0];
            this.rowSize = rowSize;
            this.columnSize = columnSize;
            this.depthSize = 0;
        }

        public void Append2DArray(int[,] array)
        {
            depthSize += 1;
            int[,,] newArray = new int[depthSize, columnSize, rowSize];

            for (int i = 0; i < (depthSize - 1); i++)
            {
                for (int j = 0; j < columnSize; j++)
                {
                    for (int k = 0; k < rowSize ; k++)
                    {
                        newArray[i, j, k] = Array[i, j, k];
                    }
                }
            }

            for (int i = 0; i < columnSize; i++)
            {
                for(int j = 0; j < rowSize; j++)
                {
                    if (i < array.GetLength(0) && j < array.GetLength(1))
                    {
                        newArray[depthSize - 1, i, j] = array[i, j];
                    }
                    else
                    {
                        newArray[columnSize - 1, i, j] = 0;
                    }
                }
            }

            Array = newArray;
        }

        public Tuple<int, int, int> GetDimensions()
        {
            return Tuple.Create(rowSize, columnSize, depthSize);
        }
    }
}
