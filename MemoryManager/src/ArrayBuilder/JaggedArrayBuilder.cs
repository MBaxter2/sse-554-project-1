﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArrayBuilder
{
    public class JaggedArrayBuilder
    {
        public int[][] Array;

        private int size;

        public JaggedArrayBuilder()
        {
            size = 0;
        }

        public void AppendRow(int[] row)
        {
            size += 1;
            int[][] newArray = new int[size][];

            for (int i = 0; i < (size - 1); i++)
            {
                newArray[i] = new int[Array[i].Length];

                for (int j = 0; j < Array[i].Length; j++)
                {
                    newArray[i][j] = Array[i][j];
                }
            }

            newArray[size-1] = new int[row.Length];
            for (int i = 0; i < row.Length; i++)
            {
                newArray[size - 1][i] = row[i];
            }

            Array = newArray;
        }
    }
}
