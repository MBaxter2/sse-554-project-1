﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace IpAddressBits
{
    public class IpDecoder
    {
        public readonly BitArray addressBits;
        public readonly BitArray subnetBits;
        public readonly BitArray maskedBits;

        public IpDecoder(string address, string subnet)
        {
            addressBits = Decode(address);
            subnetBits = Decode(subnet);

            maskedBits = new BitArray(subnetBits);
            maskedBits.Not();
            maskedBits.And(addressBits);
        }

        BitArray Decode(string input)
        {
            List<int> parts = new List<int>();
            const string pattern = @"[0-9]{1,3}";
            MatchCollection matches = Regex.Matches(input, pattern);

            foreach(Match match in matches)
            {
                int value;
                if(Int32.TryParse(match.ToString(), out value))
                {
                    parts.Add(value);
                }                
            }

            if(parts.Count() != 4)
            {
                throw new ArgumentException("Invalid input");
            }

            int[] a = { 0 };
            for(int i = 0; i < parts.Count(); i++)
            {
                a[0] |= (parts[i] & 0xFF) << (i * 8);
            }

            BitArray ba1 = new BitArray(a);
            BitArray ba2 = new BitArray(32);
            for(int i = 0; i < ba1.Count; i++)
            {
                int j = SwapIndex(i);
                ba2[j] = ba1[i];
            }

            return ba2;
        }

        public int SwapIndex(int i)
        {
            return (8) * ((i/8) + 1) - ((i % 8) + 1);
        }

        public string BitArrayToStr(BitArray input)
        {
            StringBuilder sb = new StringBuilder();

            for(int i = 0; i < input.Count; i++)
            {
                if((i != 0) && (i % 8 == 0))
                {
                    sb.Append(".");
                }
                char c = input[i] ? '1' : '0';
                sb.Append(c);
            }

            return sb.ToString();
        }
    }
}
