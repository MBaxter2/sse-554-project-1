﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Windows;

namespace IpAddressBits
{
    public class Manager : INotifyPropertyChanged
    {
        private string address;
        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged(nameof(Address));
            }
        }

        private string subnet;
        public string Subnet
        {
            get { return subnet; }
            set
            {
                subnet = value;
                OnPropertyChanged(nameof(Subnet));
            }
        }
        
        private string addressBits;
        public string AddressBits
        {
            get { return addressBits; }
            set
            {
                addressBits = value;
                OnPropertyChanged(nameof(AddressBits));
            }
        }

        private string subNetBits;
        public string SubNetBits
        {
            get { return subNetBits; }
            set
            {
                subNetBits = value;
                OnPropertyChanged(nameof(SubNetBits));
            }
        }

        private string maskedBits;
        public string MaskedBits
        {
            get { return maskedBits; }
            set
            {
                maskedBits = value;
                OnPropertyChanged(nameof(MaskedBits));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Manager()
        {
            address = "192.168.1.1";
            subnet = "255.255.255.0";
            addressBits = "";
            SubNetBits = "";
            MaskedBits = "";
        }

        public void Decode()
        {
            try
            {
                IpDecoder decoder = new IpDecoder(address, subnet);
                AddressBits = decoder.BitArrayToStr(decoder.addressBits);
                SubNetBits = decoder.BitArrayToStr(decoder.subnetBits);
                MaskedBits = decoder.BitArrayToStr(decoder.maskedBits);
            }
            catch (ArgumentException e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
