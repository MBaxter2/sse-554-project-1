﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using IpAddressBits;
using System.Collections;

namespace IpAddressBitsTests
{
    [TestClass]
    public class IpAddressBitsTests
    {
        [TestMethod]
        public void IpAddressBitsTests1()
        {
            IpDecoder id = new IpDecoder("0.0.0.0", "0.0.0.0");
            int a = id.SwapIndex(0);
            int b = id.SwapIndex(7);
            int c = id.SwapIndex(8);
            int d = id.SwapIndex(15);
            int e = id.SwapIndex(16);
            int f = id.SwapIndex(23);
            int g = id.SwapIndex(24);

            Assert.AreEqual(7, a);
            Assert.AreEqual(0, b);
            Assert.AreEqual(15, c);
            Assert.AreEqual(8, d);
            Assert.AreEqual(23, e);
            Assert.AreEqual(16, f);
            Assert.AreEqual(31, g);
        }

        [TestMethod]
        public void IpAddressBitsTests2()
        {
            IpDecoder id = new IpDecoder("1.1.1.1", "255.255.0.0");

            BitArray expected1 = new BitArray(32);
            expected1[7] = true;
            expected1[15] = true;
            expected1[23] = true;
            expected1[31] = true;

            BitArray expected2 = new BitArray(32);
            for(int i = 0; i < 16; i++)
            {
                expected2[i] = true;
            }

            BitArray expected3 = new BitArray(32);
            expected3[23] = true;
            expected3[31] = true;

            for(int i = 0; i < id.addressBits.Count; i++)
            {
                Assert.AreEqual(expected1[i], id.addressBits[i]);
            }

            for (int i = 0; i < id.subnetBits.Count; i++)
            {
                Assert.AreEqual(expected2[i], id.subnetBits[i]);
            }

            for (int i = 0; i < id.maskedBits.Count; i++)
            {
                Assert.AreEqual(expected3[i], id.maskedBits[i]);
            }
        }

        [TestMethod]
        public void IpAddressBitsTests3()
        {
            IpDecoder id = new IpDecoder("1.1.1.1", "255.255.0.0");
            string actual = id.BitArrayToStr(id.addressBits);
            string expected = "00000001.00000001.00000001.00000001";

            Assert.AreEqual(expected, actual);
            
        }
    }
}
